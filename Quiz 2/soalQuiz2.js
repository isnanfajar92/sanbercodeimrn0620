/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  // Code disini

  constructor(subject, points, email) {
    this.subject = subject
    this.points = points
    this.email = email

  }
  present() {
    return 'Hasil Try Out'+'\nPelajaran : '+this.subject+'\nNilai rata-rata : '+this.points+'\nKirim ke : '+this.email;
  }
}

rapor1 = new Score("Matematika", [60, 70, 80], 'test@test.com')

var total = 0

for(var i=0; i<rapor1.points.length; i++){  
  total = total + rapor1.points[i];
}

rapor1.points = total/rapor1.points.length



console.log(rapor1.present())
/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  // code kamu di sini
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

console.log('\n')

var people = [ ["abduh@mail.com", 85.7 ], ["khairun@mail.com", 89.3], ["bondra@mail.com", 74.3], ["regi@mail.com", 91 ] ]

for(var t=0; t<people.length; t++){  
  if(people[t][1]>90)
  {
  people[t][2] = 'honour'
  }
  else if (people[t][1]>80)
  {
    people[t][2] = 'graduate'
  }
  else
  {
    people[t][2] = 'participant'
  }
}


function recapScores(email, average, predikat) {
  // code kamu di sini

    this.email = email
    this.average = average
    this.predikat = predikat

  }
var student1 = new recapScores (people[0][0],people[0][1],people[0][2])
var student2 = new recapScores (people[1][0],people[1][1],people[1][2])
var student3 = new recapScores (people[2][0],people[2][1],people[2][2])
var student4 = new recapScores (people[3][0],people[3][1],people[3][2])

console.log(student1)
console.log(student2)
console.log(student3)
console.log(student4)

