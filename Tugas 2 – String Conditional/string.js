console.log("NOMOR 1")

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
console.log(word+" ".concat(second)+" ".concat(third)+" ".concat(fourth)+" ".concat(fifth)
+" ".concat(sixth)+" ".concat(seventh)); // concat multiple variables, masih blm nemu cara lain



console.log("")
console.log("NOMOR 2")

var sentence = "I am going to be React Native Developer"; 

//Contoh penggunaan fungsi SPLIT dari regex, jauh lebih efisien
var sentence = sentence.split(" "); // SPLITTER-nya adalah spasi

//Mengambil nilai berdasarkan index dari pemisahan method split -
var exampleFirstWord = sentence[0] ; 
var secondWord = sentence[1]; 
var thirdWord = sentence[2]; // lakukan sendiri 
var fourthWord = sentence[3]; // lakukan sendiri 
var fifthWord = sentence[4]; // lakukan sendiri 
var sixthWord = sentence[5]; // lakukan sendiri 
var seventhWord = sentence[6]; // lakukan sendiri 
var eighthWord = sentence[7]; // lakukan sendiri 

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)

console.log("")
console.log("NOMOR 3")

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); // ternyata spasi tidak dianggap substring. WOW!
var thirdWord2 = sentence2.substring(15, 17); // do your own! 
var fourthWord2 = sentence2.substring(18, 20); // do your own! 
var fifthWord2 = sentence2.substring(21, 25); // do your own! 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

console.log("")
console.log("NOMOR 4")

//atas nama efisiensi, saya tidak buat var baru, toh kalimatnya sama persis
var firstWordLength = exampleFirstWord2.length
var secondWordLength = secondWord2.length
var thirdWordLength = thirdWord2.length
var fourthWordLength = fourthWord2.length
var fifthWordLength = fifthWord2.length


// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + exampleFirstWord2 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord2 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord2 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord2 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord2 + ', with length: ' + fifthWordLength); 