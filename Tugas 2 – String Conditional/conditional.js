console.log("NOMOR 1")

var player = "Bedjo"
var role = "Office Boy"

if ( player == "" ) {
    console.log("Tidak mungkin anda tidak punya nama!")
} else {
    console.log("Hai, "+player+"! Selamat datang di isekai 'World of WereWolf'!")
}
 
if ( role == "" ) {
    console.log("Anda harus punya peran di isekai ini!")
} else if ( role == "Penyihir" ) {
    console.log("Peran anda di dunia ini adalah sebagai "+role+"!")
    console.log("Anda diberikan kekuatan mata batin untuk melihat WereWolf!")
} else if ( role == "Guard" ) {
    console.log("Peran anda di dunia ini adalah sebagai "+role+"!")
    console.log("Anda harus melindungi rakyat dari serangan WereWolf!")
} else if ( role == "WereWolf" ) {
    console.log("Peran anda di dunia ini adalah sebagai "+role+"!")
    console.log("Jadilah pemangsa yang cerdik agar bisa menghabisi rakyat di isekai ini!")
} else {
    console.log("Seorang "+role+" tidak dibutuhkan di isekai ini! Pilih peran yang lain!")
}

console.log("")
console.log("NOMOR 2")

var hari = 30;  
var tahun = 2020;
var bulan = 6;

switch(bulan) {
  case 1:   { bulan = ' Januari '; break; }
  case 2:   { bulan = ' Februari '; break; }
  case 3:   { bulan = ' Maret '; break; }
  case 4:   { bulan = ' April '; break; }
  case 5:   { bulan = ' Mei '; break; }
  case 6:   { bulan = ' Juni '; break; }
  case 7:   { bulan = ' Juli '; break; }
  case 8:   { bulan = ' Agustus '; break; }
  case 9:   { bulan = ' September '; break; }
  case 10:   { bulan = ' Oktober '; break; }
  case 11:   { bulan = ' November '; break; }
  case 12:   { bulan = ' Desember '; break; }}

  
if ( hari <=31 && hari >=1 && tahun <=2200 && tahun >=1900 ) {
    console.log("Anda akan keluar dari isekai WereWolf ini pada "+hari+bulan+tahun) ;
}    
    else {
    console.log("Anda akan keluar dari isekai WereWolf ini saat Kiamat Kubro") ;
}

