console.log('NOMOR SATU')
function range(angkaPertama, angkaTerakhir)
    {
    var numbers = [];
    if(angkaPertama<=angkaTerakhir)
    {
    while(angkaPertama<=angkaTerakhir)
        {
        numbers.push(angkaPertama); angkaPertama++
        }
        return numbers
    }   

    else if(angkaPertama>=angkaTerakhir)
    {
    while(angkaPertama>=angkaTerakhir)
        {
        numbers.push(angkaPertama); angkaPertama--
        }
        return numbers
        }
    else{
        return -1
    }
    }


    console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    console.log(range(1)) // -1
    console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
    console.log(range(54, 50)) // [54, 53, 52, 51, 50]
    console.log(range()) // -1 

console.log('\nNOMOR DUA')
function rangeWithStep(angkaPertama, angkaTerakhir, step)
    {
    var numbers = [];
    if(angkaPertama<=angkaTerakhir)
    {
    while(angkaPertama<=angkaTerakhir)
        {
        numbers.push(angkaPertama); angkaPertama=angkaPertama+step
        }
        return numbers
    }   

    else if(angkaPertama>=angkaTerakhir)
    {
    while(angkaPertama>=angkaTerakhir)
        {
        numbers.push(angkaPertama); angkaPertama=angkaPertama-step
        }
        return numbers
        }
    else{
        return -1
    }
    }


    console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
    console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
    console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
    console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

    console.log('\nNOMOR TIGA')
    function sum(angkaPertama, angkaTerakhir, step)
        {
        var numbers = 0;

        if(step==null)
        {
        step=1
        }

        if(angkaTerakhir==null)
        {
        angkaTerakhir=1
        }

        if(angkaPertama==null)
        {
        angkaPertama=-1
        }

        if(angkaPertama<=angkaTerakhir)
        {
        while(angkaPertama<=angkaTerakhir)
            {
            numbers=numbers+angkaPertama; angkaPertama=angkaPertama+step
            }
            return numbers
        }   
    
        else if(angkaPertama>=angkaTerakhir)
        {
        while(angkaPertama>=angkaTerakhir)
            {
            numbers=numbers+angkaPertama; angkaPertama=angkaPertama-step
            }
            return numbers
            }
        else{
            return -1
        }
        }
    
    
        console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log('\nNOMOR EMPAT')

//contoh input
var input =
[
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    
]

function datahandling(baris)
{
return 'Nomor ID: '+input[baris][0]+
        '\nNama Lengkap: '+input[baris][1]+
        '\nTTL: '+input[baris][2]+' '+input[baris][3]+
        '\nHobi: '+input[baris][4]+
        '\n'
}

var kolom = 0

while (kolom < input.length)
{
console.log(datahandling(kolom));kolom++
}

console.log('NOMOR LIMA')

function balikKata(word)
{
var length = word.length
var balik = ''
while (length>0)
{
balik=balik.concat(word.charAt(length-1));length--
}
return balik
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log('\nNOMOR ENAM')
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung",
"21/05/1989", "Membaca"];
function dataHandling2(input)
{
input.splice(1,1,"Roman Alamsyah Elsharawy")
input.splice(2,1,'Provinsi Bandar Lampung')
input.splice(4,0,'Pria')
input.splice(5,1,'SMA Internasional Metro')

return input
}
console.log(dataHandling2(input))

var tanggal = input[3].split("/")
var bulan = tanggal[1]

switch(bulan) {
    case '01':   { bulan = ' Januari '; break; }
    case '02':   { bulan = ' Februari '; break; }
    case '03':   { bulan = ' Maret '; break; }
    case '04':   { bulan = ' April '; break; }
    case '05':   { bulan = ' Mei '; break; }
    case '06':   { bulan = ' Juni '; break; }
    case '07':   { bulan = ' Juli '; break; }
    case '08':   { bulan = ' Agustus '; break; }
    case '09':   { bulan = ' September '; break; }
    case '10':   { bulan = ' Oktober '; break; }
    case '11':   { bulan = ' November '; break; }
    case '12':   { bulan = ' Desember '; break; }}
console.log(bulan)
console.log([tanggal[2],tanggal[0],tanggal[1]])
console.log(tanggal.join("-"))
console.log(input[1].slice(0,15))