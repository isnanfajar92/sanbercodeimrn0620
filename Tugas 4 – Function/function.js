console.log('No. 1')

function teriak() {
  return "Halo Sanbers!" 
}
 
var shout = teriak();
console.log(shout) // "Halo Sanbers!" 

console.log('\nNo. 2')

function multipy(one, two) {
  return one * two
}
 
console.log(multipy(12, 4))

console.log('\nNo. 3')

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

function introduce(name, age, address, hobby) {
  return "Nama saya "+name+"\nUmur saya "+age+" tahun\nAlamat saya di "+address+"\nHobby saya adalah "+hobby
}

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
